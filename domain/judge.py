class Judge:
    """Judge -- A person who rates the Ratee.

        Args:
            name (str): Judge name.

        Attributes:
            name (str): Judge name.

    """

    def __init__(self, logger, name):
        self._log = logger.get_logger(__name__)
        self._log.info("Creating an instance of Judge.")
        self._name = name

    def get_name(self):
        """
            Returns:
                Name of the Judge.
        """
        self._log.info("Fetching judge name")
        return self._name