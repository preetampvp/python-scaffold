import logging
logging.basicConfig(level=logging.NOTSET)


class Logger:
    """
        Logging Factorypython
    """

    @staticmethod
    def get_logger(module_name):
        """
            Args:
              module_name (str): name of the module the logger is for.

            Returns:
                A logging instance
        """

        return logging.getLogger(module_name)