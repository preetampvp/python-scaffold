import unittest
from utilities import Logger


class LoggerTests(unittest.TestCase):

    def test_get_logger_returns_an_instance_of_logger(self):
        logger = Logger.get_logger("test")
        self.assertIsNotNone(logger)