import unittest
from domain import Judge
from utilities import Logger


class JudgeTests(unittest.TestCase):

    def test_creating_judge_instance_sets_name(self):
        judge = Judge(logger=Logger, name='The Judge')
        self.assertEqual('The Judge', judge._name)

    def test_get_name_returns_name_of_the_judge(self):
        judge = Judge(logger=Logger, name='The Judge')
        self.assertEqual("The Judge", judge.get_name())