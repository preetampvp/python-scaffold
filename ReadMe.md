[ ![Codeship Status for preetampvp/python-scaffold](https://codeship.com/projects/abd3e1c0-a616-0132-9b94-7e6768721930/status?branch=master)](https://codeship.com/projects/66913)

## Virtual Environments

#### Ref: 
[ref][virtualenv]

#### Install:
pip install virtualenv

#### To create a virtualenv:
virtualenv *<name of the environment>*

This will create a folder with *<name of the environment>* as the name of the folder

#### To activate an environment:
source *<name of the environment>*/bin/activate

*Once the env is active the prompt should change*

#### To keep the dependency versions consistent use 
pip freeze > requirements.txt

#### requirements.txt
To install pip packages (dependencies) in the virtual environment

pip install -r requirements.txt

#### To deactivate environment:
deactivate

### Virtualenv wrapper (not necessary just handy)
[ref][virtualenvwrapper]

#### Install:
pip install virtualenvwrapper

#### Add the following to .bash_profile
export WORKON_HOME=$HOME/.virtualenvs

export PROJECT_HOME=$HOME/Documents/WorkingDirectory/PyProjects  (path to folder where python projects are saved)

source /usr/local/bin/virtualenvwrapper.sh

#### Commands:
[ref][virtualenvcommandref]

* workon - lists all the virtual envs
* mkvirtualenv - to create a new virtual env
* mkproject - to create a project and environment at the same time (use -f if project folder already exists)
* setvirtualenvproject - bind existing vir env to a project - (setvirtualenvproject *full path to virtual env* *full path to project dir*)
* deactivate - to deactivate

## Other notes:

### Packages require for running tests and coverage:
pip install nose

pip install coverage

### File naming conventions:
the test files should start with test Eg: test_judge.py

the test methods in the test case should start with test_ Eg: test_my_lifesaver

### Nose reference:
http://nose.readthedocs.org/en/latest/usage.html

### Running nose:
nosetests -c ./nose.config 


[virtualenv]: http://docs.python-guide.org/en/latest/dev/virtualenvs/
[virtualenvwrapper]: http://virtualenvwrapper.readthedocs.org/en/latest/install.html
[virtualenvcommandref]: http://virtualenvwrapper.readthedocs.org/en/latest/command_ref.html